package com.live.streamview.fragments;


import android.app.Dialog;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.live.streamview.R;
import com.live.streamview.utils.PostHelper;
import com.live.streamview.utils.SharedPref;
import com.live.streamview.utils.UIUtils;
import com.live.streamview.utils.URLUtils;
import com.live.streamview.adapters.CardAdapter;
import com.live.streamview.models.Card;
import com.stripe.android.Stripe;
import com.stripe.android.TokenCallback;
import com.stripe.android.model.Token;
import com.stripe.android.view.CardInputWidget;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class PaymentsFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {

    private RecyclerView cardsView;
    private List<Card> cards;
    private FloatingActionButton fab;
    private ProgressBar progress;
    private RelativeLayout errorLayout;
    private CardAdapter adapter;
    private SwipeRefreshLayout swipe;

    private Stripe stripe;
    private Token tokenSync;
    private Dialog dialog;

    public PaymentsFragment() {

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        cards = new ArrayList<>();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_payments, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        cardsView = (RecyclerView) view.findViewById(R.id.cards);
        fab = (FloatingActionButton) view.findViewById(R.id.fab);
        errorLayout = (RelativeLayout) view.findViewById(R.id.error_layout);
        progress = (ProgressBar) view.findViewById(R.id.progress_bar);
        swipe = (SwipeRefreshLayout) view.findViewById(R.id.swipe);
        swipe.setOnRefreshListener(this);
        swipe.post(new Runnable() {
            @Override
            public void run() {
                getCardDetails();
            }
        });
        cardsView.setHasFixedSize(true);
        cardsView.setLayoutManager(new LinearLayoutManager(getActivity()));
        cardsView.setItemAnimator(new DefaultItemAnimator());
        adapter = new CardAdapter(getActivity(), cards);
        cardsView.setAdapter(adapter);

        stripe = new Stripe(getActivity(), getResources().getString(R.string.stripepk));

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showAddCardDialog();
            }
        });
    }

    private void showAddCardDialog(){
        dialog = new Dialog(getActivity(), R.style.AppTheme);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_add_card);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        final CardInputWidget cardInputWidget = (CardInputWidget) dialog.findViewById(R.id.card_input_widget);
        ImageButton close = (ImageButton) dialog.findViewById(R.id.close);
        Button add = (Button) dialog.findViewById(R.id.addCard);
        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                com.stripe.android.model.Card cardToSave = cardInputWidget.getCard();
                if (cardToSave == null) {
                    dialog.cancel();
                    UIUtils.showToastMsg(getContext(), "Invalid Card Data");
                }
                else{
                    dialog.cancel();
                    progress.setVisibility(View.VISIBLE);
                    addStripeCard(cardToSave);
                }
            }
        });
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.cancel();
            }
        });
        dialog.show();
    }

    private void addStripeCard(final com.stripe.android.model.Card card) {
        stripe.createToken(card, new TokenCallback() {
            @Override
            public void onError(Exception error) {

            }

            @Override
            public void onSuccess(Token token) {
                tokenSync = token;
                addCard(card);
            }
        });
    }

    private void addCard(final com.stripe.android.model.Card card) {
        new AsyncTask<JSONObject, JSONObject, JSONObject>() {

            @Override
            protected JSONObject doInBackground(JSONObject... jsonObjects) {
                JSONObject jsonObject = new JSONObject();
                try {
                    jsonObject.put("id", SharedPref.getKey(getActivity(), "ID"));
                    jsonObject.put("token", SharedPref.getKey(getActivity(), "TOKEN"));
                    jsonObject.put("number", card.getNumber());
                    jsonObject.put("month", String.valueOf(card.getExpMonth()));
                    jsonObject.put("year", String.valueOf(card.getExpYear()));
                    jsonObject.put("cvv", card.getCVC());
                    if (tokenSync != null) {
                        jsonObject.put("card_token", tokenSync.getId());
                    }
                    PostHelper postHelper = new PostHelper(getActivity());
                    Log.e("CARDDATA", jsonObject.toString());
                    return postHelper.Post(URLUtils.addCard, jsonObject.toString());
                } catch (JSONException | IOException e) {
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(JSONObject obj) {
                super.onPostExecute(obj);
                Log.e("Response", obj.toString());
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        progress.setVisibility(View.GONE);
                    }
                });
                if (obj != null &&obj.optString("success").equals("true")) {
                    cardsView.setVisibility(View.VISIBLE);
                    JSONObject card = obj.optJSONObject("data");
                    UIUtils.showToastMsg(getActivity(), obj.optString("message"));
                    SharedPref.putKey(getActivity(), "TOKEN", card.optString("token"));
                    cards.add(new Card(card.optString("card_id"), card.optString("last_four"),
                            card.optInt("is_default") == 1));
                }
                else{
                    cardsView.setVisibility(View.GONE);
//                    if (obj.optInt("error_code") == 103 || obj.optInt("error_code") == 104) {
//                        logout();
//                    }
                }
                adapter.notifyDataSetChanged();
                if (cards.size() > 0) {
                    errorLayout.setVisibility(View.GONE);
                }
            }
        }.execute();
    }

    private void getCardDetails(){
        new AsyncTask<JSONObject, JSONObject, JSONObject>() {

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                progress.setVisibility(View.VISIBLE);
            }

            @Override
            protected JSONObject doInBackground(JSONObject... jsonObjects) {
                JSONObject jsonObject = new JSONObject();
                try {
                    jsonObject.put("id", SharedPref.getKey(getActivity(), "ID"));
                    jsonObject.put("token", SharedPref.getKey(getActivity(), "TOKEN"));

                    PostHelper postHelper = new PostHelper(getActivity());
                    return postHelper.Post(URLUtils.cardDetails, jsonObject.toString());
                } catch (JSONException | IOException e) {
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(JSONObject object) {
                super.onPostExecute(object);
                Log.e("Response", object.toString());
                if (swipe.isRefreshing()) swipe.setRefreshing(false);
                progress.setVisibility(View.GONE);
                if (object != null && object.optString("success").equals("true")) {
                    JSONArray data = object.optJSONArray("data");
                    if (data != null && data.length() > 0) {
                        cardsView.setVisibility(View.VISIBLE);
                        errorLayout.setVisibility(View.GONE);
                        for (int i = 0; i < data.length(); i++) {
                            JSONObject card = data.optJSONObject(i);
                            Card scard = new Card(card.optString("card_id"), card.optString("last_four"),
                                    card.optInt("is_default") == 1);
                            cards.add(scard);
                        }
                        adapter.notifyDataSetChanged();
                    }
                    else{
                        cardsView.setVisibility(View.GONE);
                        errorLayout.setVisibility(View.VISIBLE);
                    }
                }
                else{
                    cardsView.setVisibility(View.GONE);
                    errorLayout.setVisibility(View.VISIBLE);
//                    if (object.optInt("error_code") == 103 || object.optInt("error_code") == 104) {
//                        logout();
//                    }
                }
            }
        }.execute();
    }

    @Override
    public void onRefresh() {
        if (cards!= null && cards.size() > 0) {
            cards.clear();
        }
        getCardDetails();
    }
}
