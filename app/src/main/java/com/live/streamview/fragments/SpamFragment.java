package com.live.streamview.fragments;


import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.live.streamview.activities.Login;
import com.live.streamview.activities.OnLoadMoreListener;
import com.live.streamview.activities.SingleVideoPage;
import com.live.streamview.R;
import com.live.streamview.utils.PostHelper;
import com.live.streamview.utils.SharedPref;
import com.live.streamview.utils.UIUtils;
import com.live.streamview.utils.URLUtils;
import com.live.streamview.models.SingleItemModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

public class SpamFragment extends Fragment {

    private ProgressBar progressBar;
    private RelativeLayout relativeLayout;
    private TextView header_text_type, clear_text;
    private RecyclerView recyclerView;
    private ArrayList<SingleItemModel> singleItemModels = new ArrayList<>();
    private SpamFragment.Adapter adapter;

    public SpamFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_spam, container, false);progressBar = (ProgressBar) view.findViewById(R.id.progress_bar);
        progressBar.setVisibility(View.GONE);
        relativeLayout = (RelativeLayout) view.findViewById(R.id.error_layout);
        relativeLayout.setVisibility(View.GONE);
        recyclerView = (RecyclerView) view.findViewById(R.id.my_list_view);
        header_text_type = (TextView) view.findViewById(R.id.header_text);
        clear_text = (TextView) view.findViewById(R.id.clear_all);
        header_text_type.setText("Spam Videos");

        getSpamList();

        clear_text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AsyncTask<JSONObject, JSONObject, JSONObject>() {

                    @Override
                    protected void onPreExecute() {
                        super.onPreExecute();
                        progressBar.setVisibility(View.VISIBLE);
                    }

                    @Override
                    protected JSONObject doInBackground(JSONObject... jsonObjects) {
                        JSONObject jsonObject = new JSONObject();
                        try {
                            jsonObject.put("id", SharedPref.getKey(getActivity(), "ID"));
                            jsonObject.put("token", SharedPref.getKey(getActivity(), "TOKEN"));
                            jsonObject.put("sub_profile_id", SharedPref.getKey(getActivity(), "SUB_PROFILE"));
                            jsonObject.put("status", "1");
                            PostHelper postHelper = new PostHelper(getActivity());
                            return postHelper.Post(URLUtils.removeSpamVideos, jsonObject.toString());
                        } catch (JSONException | IOException e) {
                            e.printStackTrace();
                        }
                        return null;
                    }

                    @Override
                    protected void onPostExecute(JSONObject jsonObject) {
                        super.onPostExecute(jsonObject);
                        progressBar.setVisibility(View.GONE);
                        if (jsonObject != null) {
                            if (jsonObject.optString("success").equalsIgnoreCase("true")) {
                                relativeLayout.setVisibility(View.VISIBLE);
                                clear_text.setVisibility(View.GONE);
                                header_text_type.setVisibility(View.GONE);
                                recyclerView.setVisibility(View.GONE);
                            } else {
                                UIUtils.showToastMsg(getActivity(), jsonObject.optString("error"));
                                if (jsonObject.optString("error_code").equalsIgnoreCase("104")) {
                                    Intent intent = new Intent(getActivity(), Login.class);
                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                    startActivity(intent);
                                }
                            }
                        } else {

                            UIUtils.showToast(getActivity(), R.string.con_timeout);
                        }

                    }
                }.execute();
            }
        });

        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(getActivity(), 3);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        adapter = new Adapter();
        recyclerView.setAdapter(adapter);
        return view;
    }

    private void getSpamList() {
        new AsyncTask<JSONObject, JSONObject, JSONObject>() {

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                progressBar.setVisibility(View.VISIBLE);
            }

            @Override
            protected JSONObject doInBackground(JSONObject... jsonObjects) {

                JSONObject jsonObject = new JSONObject();
                try {
                    jsonObject.put("id", SharedPref.getKey(getActivity(), "ID"));
                    jsonObject.put("token", SharedPref.getKey(getActivity(), "TOKEN"));
                    jsonObject.put("sub_profile_id", SharedPref.getKey(getActivity(), "SUB_PROFILE"));
                    jsonObject.put("skip", "0");
                    PostHelper postHelper = new PostHelper(getActivity());
                    return postHelper.Post(URLUtils.getSpamVideos, jsonObject.toString());
                } catch (JSONException | IOException e) {
                    e.printStackTrace();
                }


                return null;
            }

            @Override
            protected void onPostExecute(JSONObject jsonObject) {
                super.onPostExecute(jsonObject);
                progressBar.setVisibility(View.GONE);
                Log.e("Spam_List", "" + jsonObject);
                if (jsonObject != null) {

                    if (jsonObject.optString("success").equalsIgnoreCase("true")) {
                        JSONArray history_array = jsonObject.optJSONArray("data");
                        if (history_array.length() == 0) {
                            relativeLayout.setVisibility(View.VISIBLE);
                            clear_text.setVisibility(View.GONE);
                            header_text_type.setVisibility(View.GONE);
                            recyclerView.setVisibility(View.GONE);
                        } else {
                            for (int i = 0; i < history_array.length(); i++) {
                                JSONObject object = history_array.optJSONObject(i);
                                String admin_video_id = object.optString("admin_video_id");
                                String wishlist_id = object.optString("history_id");
                                String title = object.optString("title");
                                String category_name = object.optString("category_name");
                                String default_image = object.optString("default_image");
                                String publish_time = object.optString("publish_time");
                                singleItemModels.add(new SingleItemModel(admin_video_id, publish_time, "", title, "", default_image, category_name, wishlist_id));
                            }


                        }

                    } else {
                        UIUtils.showToastMsg(getActivity(), jsonObject.optString("error"));
                        if (jsonObject.optString("error_code").equalsIgnoreCase("104")) {
                            Intent intent = new Intent(getActivity(), Login.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent);
                        }
                    }

                } else {
                    UIUtils.showToast(getActivity(), R.string.con_timeout);
                }
            }
        }.execute();
    }

    private class Adapter extends RecyclerView.Adapter {

        private OnLoadMoreListener mOnLoadMoreListener;
        private final int VIEW_TYPE_ITEM = 0;
        private final int VIEW_TYPE_LOADING = 1;

        private boolean isLoading;
        private int visibleThreshold = 5;
        private int lastVisibleItem, totalItemCount;

        public Adapter() {
            final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
            recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);

                    totalItemCount = linearLayoutManager.getItemCount();
                    lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();

                    if (!isLoading && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                        if (mOnLoadMoreListener != null) {
                            mOnLoadMoreListener.onLoadMore();
                        }
                        isLoading = true;
                    }
                }
            });
        }

        public void setOnLoadMoreListener(OnLoadMoreListener mOnLoadMoreListener) {
            this.mOnLoadMoreListener = mOnLoadMoreListener;
        }
        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            if (viewType == VIEW_TYPE_ITEM) {
                View view = LayoutInflater.from(getActivity()).inflate(R.layout.my_wish_list_item, parent, false);
                return new History.MyViewHolder(view);
            } else if (viewType == VIEW_TYPE_LOADING) {
                View view = LayoutInflater.from(getActivity()).inflate(R.layout.progress_bar, parent, false);
                return new History.LoadingViewHolder(view);
            }
            return null;
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
            if (holder instanceof History.MyViewHolder) {
                final History.MyViewHolder myViewHolder = (History.MyViewHolder) holder;
                Glide.with(getActivity()).load(singleItemModels.get(position).
                        getDefault_image()).centerCrop().error(R.drawable.ic_profile).
                        crossFade(50).into(myViewHolder.wishImage);
                myViewHolder.wishName.setText(singleItemModels.get(position).getTitle());
                //holder.wishTime.setText(wishList.get(position).get("publish_time"));
                //holder.wishName.setText(wishList.get(position).get("title"));
                //holder.wishCate.setText(wishList.get(position).get("category_name"));

                myViewHolder.wishImage.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        String video_ID = singleItemModels.get(position).getAdmin_video_id();
                        Intent intent = new Intent(getActivity(), SingleVideoPage.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        intent.putExtra("videoID", video_ID);
                        startActivity(intent);
                    }
                });
                myViewHolder.wishImage.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View view) {
                        final String history_id = singleItemModels.get(position).getAdmin_video_id();
                        PopupMenu popupMenu = new PopupMenu(getActivity(), myViewHolder.wishImage);
                        popupMenu.getMenuInflater().inflate(R.menu.popup_menu, popupMenu.getMenu());
                        popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                            @Override
                            public boolean onMenuItemClick(MenuItem item) {
                                new AsyncTask<JSONObject, JSONObject, JSONObject>() {

                                    @Override
                                    protected void onPreExecute() {
                                        super.onPreExecute();
                                        progressBar.setVisibility(View.VISIBLE);
                                    }

                                    @Override
                                    protected JSONObject doInBackground(JSONObject... jsonObjects) {
                                        JSONObject jsonObject = new JSONObject();
                                        try {
                                            jsonObject.put("id", SharedPref.getKey(getActivity(), "ID"));
                                            jsonObject.put("token", SharedPref.getKey(getActivity(), "TOKEN"));
                                            jsonObject.put("sub_profile_id", SharedPref.getKey(getActivity(), "SUB_PROFILE"));
                                            jsonObject.put("admin_video_id", history_id);
                                            Log.e("removePost", "" + jsonObject);
                                            PostHelper postHelper = new PostHelper(getActivity());
                                            return postHelper.Post(URLUtils.removeSpamVideos, jsonObject.toString());
                                        } catch (JSONException | IOException e) {
                                            e.printStackTrace();
                                        }
                                        return null;
                                    }

                                    @Override
                                    protected void onPostExecute(JSONObject object) {
                                        super.onPostExecute(object);
                                        progressBar.setVisibility(View.GONE);
                                        if (object != null) {
                                            if (object.optBoolean("success")) {
                                                singleItemModels.remove(position);
                                                notifyDataSetChanged();
                                            } else {
                                                UIUtils.showToastMsg(getActivity(), object.optString("error"));
                                                if (object.optString("error_code").equalsIgnoreCase("104")) {
                                                    Intent intent = new Intent(getActivity(), Login.class);
                                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                                    startActivity(intent);
                                                }
                                            }
                                        } else {
                                            UIUtils.showToast(getActivity(), R.string.con_timeout);
                                        }
                                    }
                                }.execute();
                                return false;
                            }
                        });
                        popupMenu.show();
                        return false;
                    }
                });
            } else if (holder instanceof History.LoadingViewHolder) {
                History.LoadingViewHolder loadingViewHolder = (History.LoadingViewHolder) holder;
                loadingViewHolder.progressBar.setIndeterminate(true);
            }
        }

        @Override
        public int getItemCount() {
            return singleItemModels == null ? 0 : singleItemModels.size();
        }

        @Override
        public int getItemViewType(int position) {
            return singleItemModels.get(position) == null ? VIEW_TYPE_LOADING : VIEW_TYPE_ITEM;
        }

        public void setLoaded() {
            isLoading = false;
        }
    }
}
