package com.live.streamview;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerView;
import com.live.streamview.utils.PostHelper;
import com.live.streamview.utils.SharedPref;
import com.live.streamview.utils.URLUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;


public class FullVideoActivity extends YouTubeBaseActivity implements YouTubePlayer.OnInitializedListener,
                                            YouTubePlayer.PlayerStateChangeListener{

    YouTubePlayerView youTubePlayer;
    private String video_URL = "";
    private String API = "AIzaSyAw1nNVn8q5OShMOsfUWDHR9ESOLLDkZas";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_full_video);


        youTubePlayer = (YouTubePlayerView) findViewById(R.id.youtube_view);
        ImageView back = (ImageView) findViewById(R.id.back_arrow);
        back.setVisibility(View.GONE);

        Intent intent = getIntent();
        if (intent != null) {
            video_URL = intent.getStringExtra("video_URL");
            String video_type = intent.getStringExtra("video_type");
            if (video_type.equalsIgnoreCase("1") || video_type.equalsIgnoreCase("3")) {
                youTubePlayer.setVisibility(View.GONE);
                this.finish();
            }
            else if (video_type.equalsIgnoreCase("2")) {
                back.setVisibility(View.VISIBLE);
                youTubePlayer.setVisibility(View.VISIBLE);
                youTubePlayer.initialize(API, FullVideoActivity.this);
            }

        }

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1) {
            // Retry initialization if user performed a recovery action
            getYouTubePlayerProvider().initialize(API, this);
        }
    }

    private YouTubePlayer.Provider getYouTubePlayerProvider() {
        return (YouTubePlayerView) findViewById(R.id.youtube_view);
    }


    @Override
    public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer youTubePlayer, boolean b) {
        youTubePlayer.cueVideo(video_URL);
    }

    @Override
    public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult youTubeInitializationResult) {
        Toast.makeText(this, "Failured to Initialize!", Toast.LENGTH_LONG).show();
        if (youTubeInitializationResult.isUserRecoverableError()) {
            youTubeInitializationResult.getErrorDialog(this, 1).show();
        } else {
            @SuppressLint({"StringFormatInvalid", "LocalSuppress"}) String errorMessage = String.format(
                    getString(R.string.error_player), youTubeInitializationResult.toString());
            Toast.makeText(this, errorMessage, Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public void onLoading() {

    }

    @Override
    public void onLoaded(String s) {

    }

    @Override
    public void onAdStarted() {

    }

    @Override
    public void onVideoStarted() {

    }

    @Override
    public void onVideoEnded() {
        makeACall();
    }

    private void makeACall() {
        new AsyncTask<JSONObject, JSONObject, JSONObject>() {
            @Override
            protected JSONObject doInBackground(JSONObject... jsonObjects) {
                JSONObject jsonObject = new JSONObject();
                try {
                    jsonObject.put("id", SharedPref.getKey(FullVideoActivity.this, "ID"));
                    jsonObject.put("token", SharedPref.getKey(FullVideoActivity.this, "TOKEN"));
                    jsonObject.put("admin_video_id", getIntent().getStringExtra("admin_video_id"));

                    PostHelper postHelper = new PostHelper(FullVideoActivity.this);
                    return postHelper.Post(URLUtils.payPerViewEnd, jsonObject.toString());

                } catch (JSONException | IOException e) {
                    e.printStackTrace();
                }
                return null;
            }
        }.execute();
    }

    @Override
    public void onError(YouTubePlayer.ErrorReason errorReason) {

    }
}
