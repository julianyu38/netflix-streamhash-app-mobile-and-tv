package com.live.streamview.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.live.streamview.activities.AddProfileActivity;
import com.live.streamview.activities.MainActivity;
import com.live.streamview.activities.ManageProfileActivity;
import com.live.streamview.R;
import com.live.streamview.utils.SharedPref;
import com.live.streamview.models.User;
import com.mikhaellopez.circularimageview.CircularImageView;

import java.util.List;

/**
 * Created by codegama on 16/10/17.
 */

public class UserGridAdapter extends RecyclerView.Adapter<UserGridAdapter.ViewHolder>{

    private Context context;
    private List<User> users;
    private LayoutInflater inflater;
    private boolean isEditMode = false;

    public UserGridAdapter(Context context, List<User> users) {
        this.context = context;
        this.users = users;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.view_grid_user, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final User user = users.get(position);
        if (user != null) {
            holder.name.setText(user.getName());
            Glide.with(context).load(user.getImage()).centerCrop().diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(holder.image);
            if (!user.getId().equals("add")) {
                if (isEditMode) {
                    holder.image.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Intent intent = new Intent(context, AddProfileActivity.class);
                            intent.putExtra("edit", true);
                            intent.putExtra("id", user.getId());
                            intent.putExtra("name", user.getName());
                            intent.putExtra("picture", user.getImage());
                            context.startActivity(intent);
                            ((ManageProfileActivity) context).finish();
                        }
                    });
                }
                else {
                    holder.image.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            SharedPref.putKey(context, "NAME", user.getName());
                            SharedPref.putKey(context, "PROFILE_IMAGE", user.getImage());
                            SharedPref.putKey(context, "SUB_PROFILE", user.getId());
                            LocalBroadcastManager.getInstance(context).sendBroadcast(new Intent(MainActivity.UIUpdateEventFilter));
                            ((ManageProfileActivity) context).finish();
                        }
                    });
                }
            }
            else {
                holder.image.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        context.startActivity(new Intent(context, AddProfileActivity.class));
                        ((ManageProfileActivity) context).finish();
                    }
                });
            }
        }
    }

    public void setEditMode(boolean isEditMode) {
        this.isEditMode = isEditMode;
    }

    @Override
    public int getItemCount() {
        return users.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public CircularImageView image;
        public TextView name;

        public ViewHolder(View itemView) {
            super(itemView);
            image = (CircularImageView) itemView.findViewById(R.id.image);
            name = (TextView) itemView.findViewById(R.id.name);
        }
    }
}
