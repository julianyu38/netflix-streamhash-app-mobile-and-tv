package com.live.streamview.adapters;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.live.streamview.R;
import com.live.streamview.models.Subscription;

import java.util.List;

/**
 * Created by codegama on 14/10/17.
 */

public class SubscriptionAdapter extends RecyclerView.Adapter<SubscriptionAdapter.ViewHolder>{

    private Context context;
    private List<Subscription> subscriptions;
    private LayoutInflater inflater;
    OnSubscriptionInterface listener;

    public SubscriptionAdapter(Context context, List<Subscription> subscriptions) {
        this.context = context;
        this.subscriptions = subscriptions;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.view_subscription, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final Subscription subscription = subscriptions.get(position);
        if (subscription != null) {
            if (subscription.getStatus().equals("1")) {
                holder.card.setBackgroundColor(ContextCompat.getColor(context, R.color.colorAccent));
                holder.pay.setBackground(ContextCompat.getDrawable(context, R.drawable.button_red));
            }
            else {
                holder.card.setBackgroundColor(ContextCompat.getColor(context, R.color.colorPrimary));
                holder.pay.setBackground(ContextCompat.getDrawable(context, R.drawable.button_black));
            }
            holder.title.setText(subscription.getTitle());
            holder.price.setText(subscription.getAmount());
            holder.months.setText(subscription.getMonths());
            holder.description.setText(subscription.getDescription());
            holder.accounts.setText(subscription.getAccounts());
            holder.expires.setText(subscription.getExpires());

            if (subscription.isMine()) {
                holder.pay.setVisibility(View.GONE);
            }
            else{
                holder.pay.setVisibility(View.VISIBLE);
            }
            holder.pay.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (listener != null) {
                        listener.showDialog(subscription);
                    }
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return subscriptions.size();
    }

    public interface OnSubscriptionInterface {
        void showDialog(Subscription subscription);
    }

    public void setOnSubscriptionListener(OnSubscriptionInterface listener){
        this.listener = listener;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        public LinearLayout card;
        public TextView title, description, price, months, accounts, expires;
        public Button pay;

        public ViewHolder(View itemView) {
            super(itemView);
            card = (LinearLayout) itemView.findViewById(R.id.background);
            title = (TextView) itemView.findViewById(R.id.title);
            description = (TextView) itemView.findViewById(R.id.description);
            price = (TextView) itemView.findViewById(R.id.price);
            months = (TextView) itemView.findViewById(R.id.months);
            accounts = (TextView) itemView.findViewById(R.id.accounts);
            expires = (TextView) itemView.findViewById(R.id.expires);
            pay = (Button) itemView.findViewById(R.id.pay);
        }
    }
}
