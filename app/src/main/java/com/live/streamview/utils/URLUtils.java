package com.live.streamview.utils;

public class URLUtils {
    //public static String base = "http://startstreaming.info/";
    //public static String base = "http://default.startstreaming.info/";
    public static String base = "http://admin.cinemabox.tv/";

    public static String login = base + "userApi/login";
    public static String register = base + "userApi/register";
    public static String forgot_password = base + "userApi/forgotpassword";
    public static String profile_update = base + "userApi/updateProfile";
    public static String change_Password = base + "userApi/changePassword";
    public static String categories = base + "userApi/categories";
    public static String home = base + "userApi/home";
    public static String categoryVideos = base + "userApi/categoryVideos";
    public static String getWishlist = base + "userApi/getWishlist";
    public static String deleteWishlist = base + "userApi/deleteWishlist";
    public static String addWishlist = base + "userApi/addWishlist";
    public static String getHistory = base + "userApi/getHistory";
    public static String deleteHistory = base + "userApi/deleteHistory";
    public static String singleVideo = base + "userApi/singleVideo";
    public static String userRating = base + "userApi/userRating";
    public static String searchVideo = base + "userApi/apiSearchVideo";
    public static String deleteAccount = base + "userApi/deleteAccount";
    public static String subCategoryVideos = base + "userApi/keyBasedDetails";
    public static String addHistory = base + "userApi/addHistory";
    public static String settings = base + "userApi/settings";
    public static String addCard = base + "userApi/payment_card_add";
    public static String defaultCard = base + "userApi/default_card";
    public static String deleteCard = base + "userApi/delete_card";
    public static String cardDetails = base + "userApi/card_details";
    public static String viewPlans = base + "userApi/subscription_plans";
    public static String myPlans = base + "userApi/subscribedPlans";
    public static String stripePayment = base + "userApi/stripe_payment";
    public static String paypalPayment = base + "userApi/pay_now";
    public static String activeProfiles = base + "userApi/active-profiles";
    public static String addProfile = base + "userApi/add-profile";
    public static String editSubProfile = base + "userApi/edit-sub-profile";
    public static String deleteSubProfile = base + "userApi/delete-sub-profile";
    public static String likeVideos = base + "userApi/like_video";
    public static String disLikeVideos = base + "userApi/dis_like_video";
    public static String stripePayPerView = base + "userApi/stripe_ppv";
    public static String paypalPayPerView = base + "userApi/paypal_ppv";
    public static String payPerViewEnd = base + "userApi/ppv_end";
    public static String getSpamVideos = base + "userApi/spam_videos";
    public static String addSpamVideos = base + "userApi/add_spam";
    public static String removeSpamVideos = base + "userApi/remove_spam";
    public static String spamReasons = base + "userApi/spam-reasons";

    public class PayPalConfig {

        public static final String PAYPAL_CLIENT_ID = "AaXkweZD5g9s0X3BsO0Y4Q-kNzbmLZaog0mbmVGrTT5IX0O73LoLVcHp17e6pkG7Vm04JEUuG6up30LD";
    }

}
