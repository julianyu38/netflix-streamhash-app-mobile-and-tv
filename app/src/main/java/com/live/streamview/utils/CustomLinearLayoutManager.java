package com.live.streamview.utils;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;

/**
 * Created by codegama on 11/10/17.
 */

public class CustomLinearLayoutManager extends LinearLayoutManager {

    public CustomLinearLayoutManager(Context context, int orientation, boolean reverseLayout) {
        super(context, orientation, reverseLayout);
    }


    @Override
    public boolean canScrollHorizontally() {
        return false;
    }
}


