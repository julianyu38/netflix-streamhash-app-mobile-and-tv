package com.live.streamview;

import android.content.DialogInterface;
import android.graphics.Color;
import android.media.AudioManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.Surface;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.WindowManager;
import android.widget.TextView;

import com.google.android.exoplayer2.C;
import com.google.android.exoplayer2.DefaultLoadControl;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.Format;
import com.google.android.exoplayer2.LoadControl;
import com.google.android.exoplayer2.PlaybackParameters;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.decoder.DecoderCounters;
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory;
import com.google.android.exoplayer2.extractor.ExtractorsFactory;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.MergingMediaSource;
import com.google.android.exoplayer2.source.SingleSampleMediaSource;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelection;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.trackselection.TrackSelector;
import com.google.android.exoplayer2.ui.SimpleExoPlayerView;
import com.google.android.exoplayer2.upstream.BandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.util.MimeTypes;
import com.google.android.exoplayer2.util.Util;
import com.google.android.exoplayer2.video.VideoRendererEventListener;
import com.live.streamview.utils.PostHelper;
import com.live.streamview.utils.SharedPref;
import com.live.streamview.utils.URLUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

public class ExoPlayerActivity extends AppCompatActivity implements VideoRendererEventListener{

    private static final String TAG = "ExoPlayerActivity";
    private SimpleExoPlayerView exoPlayerView;
    private SimpleExoPlayer player;
    private String videoUrl;
    private String subtitle;
    private TextView duration;
    private Toolbar toolbar;

    private Format subTitleFormat;
    private boolean isSubTitle = false;
    private boolean isFullScreen = false;
    private MergingMediaSource mergingMediaSource;
    private MediaSource videoSource;
    private MediaSource subTitleSource;

    private AudioManager audioManager;
    private int position;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        setContentView(R.layout.activity_exo_player);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        SpannableString s = new SpannableString(getIntent().getStringExtra("video_name"));
        s.setSpan(new ForegroundColorSpan(Color.WHITE), 0,
                getIntent().getStringExtra("video_name").length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        getSupportActionBar().setTitle(s);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        audioManager = (AudioManager) getSystemService(AUDIO_SERVICE);

        exoPlayerView = (SimpleExoPlayerView) findViewById(R.id.exoplayer);
        duration = (TextView) findViewById(R.id.exo_duration);
        videoUrl = getIntent().getStringExtra("video_URL");
        subtitle = getIntent().getStringExtra("video_subtitle");

        if (!subtitle.isEmpty()) {
            isSubTitle = true;
            subTitleFormat = Format.createTextSampleFormat(null,
                    MimeTypes.APPLICATION_SUBRIP,
                    1,
                    null);
        }

        BandwidthMeter bandwidthMeter = new DefaultBandwidthMeter();
        TrackSelection.Factory factory = new AdaptiveTrackSelection.Factory(bandwidthMeter);
        TrackSelector selector = new DefaultTrackSelector(factory);

        LoadControl loadControl = new DefaultLoadControl();

        player = ExoPlayerFactory.newSimpleInstance(this, selector, loadControl);

        exoPlayerView.setUseController(true);
        exoPlayerView.requestFocus();
        exoPlayerView.setPlayer(player);

        Uri video = Uri.parse(videoUrl);

        DefaultBandwidthMeter bandwidthMeterA = new DefaultBandwidthMeter();
        DefaultDataSourceFactory dataSourceFactory = new DefaultDataSourceFactory(this, Util.getUserAgent(this, "StreamView"), bandwidthMeterA);
        ExtractorsFactory extractorsFactory = new DefaultExtractorsFactory();

        videoSource = new ExtractorMediaSource(video, dataSourceFactory, extractorsFactory, null, null);

        subTitleSource = new SingleSampleMediaSource(Uri.parse(subtitle), dataSourceFactory, subTitleFormat, C.TIME_UNSET);

        if (isSubTitle) {
            mergingMediaSource = new MergingMediaSource(videoSource, subTitleSource);
            player.prepare(mergingMediaSource);
        }
        else {
            player.prepare(videoSource);
        }

        player.addListener(new Player.EventListener() {

            @Override
            public void onTimelineChanged(Timeline timeline, Object manifest) {
                Log.v(TAG, "Listener-onTimelineChanged...");
            }

            @Override
            public void onTracksChanged(TrackGroupArray trackGroups, TrackSelectionArray trackSelections) {
                Log.v(TAG, "Listener-onTracksChanged...");
            }

            @Override
            public void onLoadingChanged(boolean isLoading) {
                Log.v(TAG, "Listener-onLoadingChanged...isLoading:"+isLoading);
            }

            @Override
            public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
                Log.v(TAG, "Listener-onPlayerStateChanged..." + playbackState);
                switch (playbackState) {
                    case Player.STATE_BUFFERING:
                        break;
                    case Player.STATE_ENDED:
                        makeACall();
                        break;
                }
            }

            @Override
            public void onRepeatModeChanged(int repeatMode) {
                Log.v(TAG, "Listener-onRepeatModeChanged...");
            }

            @Override
            public void onPlayerError(ExoPlaybackException error) {
                Log.v(TAG, "Listener-onPlayerError...");
                player.stop();
                player.prepare(isSubTitle ? mergingMediaSource : videoSource);
                player.setPlayWhenReady(true);
            }

            @Override
            public void onPositionDiscontinuity() {
                Log.v(TAG, "Listener-onPositionDiscontinuity...");
            }

            @Override
            public void onPlaybackParametersChanged(PlaybackParameters playbackParameters) {
                Log.v(TAG, "Listener-onPlaybackParametersChanged...");
            }
        });

        player.setPlayWhenReady(true);
        player.setVideoDebugListener(this);

    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        toggleFullScreen();
        return true;
    }

    private void toggleFullScreen() {
        if (isFullScreen) {
            showSystemUI();
            isFullScreen = false;
        }
        else {
            hideSystemUI();
            isFullScreen = true;
        }
    }

    private void hideSystemUI(){
        getSupportActionBar().hide();
    }

    private void showSystemUI(){
        getSupportActionBar().show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_exo, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
            case R.id.action_subtitle:
                showSubTitlesDialog();
                break;
            case R.id.action_volume:
                audioManager.setStreamVolume(AudioManager.STREAM_MUSIC,
                        audioManager.getStreamVolume(AudioManager.STREAM_MUSIC),
                                        AudioManager.FLAG_SHOW_UI);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void showSubTitlesDialog() {
        CharSequence[] items;
        if (!subtitle.isEmpty()) {
            items = new CharSequence[] {"Off", "English[CC]"};
            position = 1;
        }
        else {
            items = new CharSequence[] {"Off"};
            position = 0;
        }
        new AlertDialog.Builder(this, R.style.AppTheme_Dialog)
                .setTitle("Subtitles")
                .setSingleChoiceItems(items, isSubTitle ? 1 : 0, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        position = i;
                    }
                })
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        toggleSubTitle(position == 0);
                    }
                })
                .create().show();
    }

    private void toggleSubTitle(boolean isSubTitle) {
        if (isSubTitle && this.isSubTitle) {
            this.isSubTitle = false;
            long duration = player.getCurrentPosition();
            player.stop();
            player.prepare(videoSource, true, true);
            player.seekTo(duration);
            player.setPlayWhenReady(true);
        }
        else if (!isSubTitle && !this.isSubTitle){
            this.isSubTitle = true;
            long duration = player.getCurrentPosition();
            player.stop();
            player.prepare(mergingMediaSource, true , true);
            player.seekTo(duration);
            player.setPlayWhenReady(true);
        }
    }

    @Override
    public void onVideoEnabled(DecoderCounters counters) {

    }

    @Override
    public void onVideoDecoderInitialized(String decoderName, long initializedTimestampMs, long initializationDurationMs) {

    }

    @Override
    public void onVideoInputFormatChanged(Format format) {

    }

    @Override
    public void onDroppedFrames(int count, long elapsedMs) {

    }

    @Override
    public void onVideoSizeChanged(int width, int height, int unappliedRotationDegrees, float pixelWidthHeightRatio) {
        Log.v(TAG, "onVideoSizeChanged ["  + " width: " + width + " height: " + height + "]");
    }

    @Override
    public void onRenderedFirstFrame(Surface surface) {

    }

    @Override
    public void onVideoDisabled(DecoderCounters counters) {

    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.v(TAG, "onStop()...");
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.v(TAG, "onStart()...");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.v(TAG, "onResume()...");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.v(TAG, "onPause()...");
    }

    private void makeACall() {
        new AsyncTask<JSONObject, JSONObject, JSONObject>() {
            @Override
            protected JSONObject doInBackground(JSONObject... jsonObjects) {
                JSONObject jsonObject = new JSONObject();
                try {
                    jsonObject.put("id", SharedPref.getKey(ExoPlayerActivity.this, "ID"));
                    jsonObject.put("token", SharedPref.getKey(ExoPlayerActivity.this, "TOKEN"));
                    jsonObject.put("admin_video_id", getIntent().getStringExtra("admin_video_id"));

                    PostHelper postHelper = new PostHelper(ExoPlayerActivity.this);
                    return postHelper.Post(URLUtils.payPerViewEnd, jsonObject.toString());

                } catch (JSONException | IOException e) {
                    e.printStackTrace();
                }
                return null;
            }
        }.execute();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.v(TAG, "onDestroy()...");
        player.release();
    }
}
