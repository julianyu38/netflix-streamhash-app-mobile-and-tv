package com.live.streamview.activities;

/**
 * Created by KrishnaDev on 10/22/16.
 */
public interface OnLoadMoreListener {
    void onLoadMore();
}
