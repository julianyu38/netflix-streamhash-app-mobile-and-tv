package com.live.streamview.activities;

import android.content.ComponentName;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.live.streamview.R;
import com.live.streamview.utils.ConnectionHelper;
import com.live.streamview.utils.PostHelper;
import com.live.streamview.utils.SharedPref;
import com.live.streamview.utils.UIUtils;
import com.live.streamview.utils.URLUtils;
import com.mikhaellopez.circularimageview.CircularImageView;
import com.yalantis.ucrop.UCrop;
import com.yalantis.ucrop.UCropActivity;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class AddProfileActivity extends AppCompatActivity {

    private Button save, cancel;
    private CircularImageView image;
    private EditText et_name;
    private String imagePath = "", name = "";
    private Uri imageUri;
    private static final int IMAGE_REQUEST_CODE = 100;
    private ProgressBar progressBar;
    MultipartEntity entityBuilder;
    private boolean isEditMode = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_profile);

        save = (Button) findViewById(R.id.save);
        cancel = (Button) findViewById(R.id.cancel);
        image = (CircularImageView) findViewById(R.id.image);
        et_name = (EditText) findViewById(R.id.name);
        progressBar = (ProgressBar) findViewById(R.id.progress_bar);

        if (getIntent().hasExtra("edit")) {
            isEditMode = true;
            et_name.setText(getIntent().getStringExtra("name"));
            Glide.with(this).load(getIntent().getStringExtra("picture")).centerCrop().diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(image);
            findViewById(R.id.deleteLayout).setVisibility(View.VISIBLE);
        }
        else
            findViewById(R.id.deleteLayout).setVisibility(View.GONE);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loadImage();
            }
        });

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ConnectionHelper.isConnectingToInternet(AddProfileActivity.this)) {
                    if (et_name.getText().toString().trim().length() > 0) {
                        name = et_name.getText().toString().trim();
                        addProfile();
                    }
                    else {
                        UIUtils.showToastMsg(AddProfileActivity.this, "Name Required to Add");
                    }
                }
                else {
                    UIUtils.showToastMsg(AddProfileActivity.this, "Please connect to Internet");
                }
            }
        });

        findViewById(R.id.deleteLayout).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new AlertDialog.Builder(AddProfileActivity.this)
                        .setMessage("Are you sure to Delete your Account?")
                        .setPositiveButton("YES", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.cancel();
                                deleteThis();
                            }
                        })
                        .setNegativeButton("NO", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.cancel();
                            }
                        })
                        .create().show();
            }
        });
    }

    private void deleteThis() {
        if (ConnectionHelper.isConnectingToInternet(this)) {
            new AsyncTask<JSONObject, JSONObject, JSONObject>() {
                @Override
                protected void onPreExecute() {
                    super.onPreExecute();
                    progressBar.setVisibility(View.VISIBLE);
                }

                @Override
                protected JSONObject doInBackground(JSONObject... jsonObjects) {
                    JSONObject jsonObject = new JSONObject();
                    try {
                        jsonObject.put("id", SharedPref.getKey(AddProfileActivity.this, "ID"));
                        jsonObject.put("token", SharedPref.getKey(AddProfileActivity.this, "TOKEN"));
                        jsonObject.put("sub_profile_id", getIntent().getStringExtra("id"));

                        PostHelper postHelper = new PostHelper(AddProfileActivity.this);
                        return postHelper.Post(URLUtils.deleteSubProfile, jsonObject.toString());

                    } catch (JSONException | IOException e) {
                        e.printStackTrace();
                    }
                    return null;
                }

                @Override
                protected void onPostExecute(JSONObject object) {
                    super.onPostExecute(object);
                    progressBar.setVisibility(View.GONE);
                    Log.e("Profiles", object.toString());
                    if (object != null) {
                        if (object.optString("success").equalsIgnoreCase("true")) {
                            onBackPressed();
                        }
                        else {
                            UIUtils.showToastMsg(AddProfileActivity.this, object.optString("error"));
                            if (object.optString("error_code").equalsIgnoreCase("104")) {
                                Intent intent = new Intent(AddProfileActivity.this, Login.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                startActivity(intent);
                            }
                        }
                    } else {
                        UIUtils.showToast(AddProfileActivity.this, R.string.con_timeout);
                    }
                }
            }.execute();
        }
        else {
            UIUtils.showToastMsg(this, "Need to Connect to the Internet");
            return;
        }
    }

    private void loadImage(){
        final File root = new File(Environment.getExternalStorageDirectory() + File.separator + "StreamView" + File.separator);
        if (!root.exists()){
            root.mkdirs();
        }
        final String fname = getUniqueImageFileName();
        File main = new File(root, fname);
        imageUri = Uri.fromFile(main);

        List<Intent> cameraIntents = new ArrayList<>();
        Intent captureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        PackageManager packageManager = getPackageManager();
        List<ResolveInfo> listCam = packageManager.queryIntentActivities(captureIntent, 0);
        for (ResolveInfo info : listCam){
            String packageName = info.activityInfo.packageName;
            Intent intent = new Intent(captureIntent);
            intent.setComponent(new ComponentName(packageName, info.activityInfo.name));
            intent.setPackage(packageName);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
            cameraIntents.add(intent);
        }

        Intent gallery = new Intent();
        gallery.setType("image/*");
        gallery.setAction(Intent.ACTION_GET_CONTENT);

        Intent chooserIntent = Intent.createChooser(gallery, "Select Source");
        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, cameraIntents.toArray(new Parcelable[cameraIntents.size()]));
        startActivityForResult(chooserIntent, IMAGE_REQUEST_CODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case IMAGE_REQUEST_CODE:
                boolean isCamera;
                if (data == null) isCamera = true;
                else {
                    String action = data.getAction();
                    if (action == null) {
                        isCamera = false;
                    } else {
                        isCamera = action.equals(MediaStore.ACTION_IMAGE_CAPTURE);
                    }
                }
                Uri filePath;
                if (isCamera) {
                    filePath = imageUri;
                } else {
                    filePath = data.getData();
                }
                beginCrop(filePath);
                break;
            case UCrop.REQUEST_CROP:
                handleCropResult(data);
                break;
            case UCrop.RESULT_ERROR:
                handleCropError(data);
                break;
        }
    }

    private void beginCrop(Uri source){
        Uri outputUri = Uri.fromFile(new File(Environment.getExternalStorageDirectory() + File.separator + "StreamView" + File.separator,
                "CROP" + (Calendar.getInstance().getTimeInMillis() + ".jpg")));
        UCrop.Options options = new UCrop.Options();
        options.setHideBottomControls(true);
        options.setAllowedGestures(UCropActivity.SCALE, UCropActivity.ROTATE, UCropActivity.ALL);
        options.setShowCropFrame(true);
        options.setCropGridColor(ContextCompat.getColor(this, R.color.white));
        options.setToolbarColor(ContextCompat.getColor(this, R.color.colorPrimary));
        options.setStatusBarColor(ContextCompat.getColor(this, R.color.colorPrimaryDark));
        UCrop.of(source, outputUri).withAspectRatio(1, 1).withOptions(options).start(this);
    }

    private void handleCropResult( Intent result) {
        if (result == null ) return;
        final Uri resultUri = UCrop.getOutput(result);
        if (resultUri != null) {
            imagePath = getRealPathFromURI(resultUri);
            image.setBackground(null);
            Glide.with(this).load(imagePath).into(image);
        }
    }

    private String getRealPathFromURI(Uri contentURI){
        String result;
        Cursor cursor = getContentResolver().query(contentURI, null, null, null, null);
        if (cursor == null) result = contentURI.getPath();
        else{
            cursor.moveToFirst();
            int index = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            result = cursor.getString(index);
            cursor.close();
        }
        return result;
    }

    private void handleCropError(@NonNull Intent result) {
        final Throwable cropError = UCrop.getError(result);
        if (cropError != null) {
            Log.e("CROPERROR", "handleCropError: ", cropError);
        }
    }

    public static String getUniqueImageFileName() {
        Calendar cal = Calendar.getInstance();
        return "IMG_" + cal.getTimeInMillis() + ".jpg";
    }

    private void addProfile() {
        new AsyncTask<String, Void, String>() {

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                progressBar.setVisibility(View.VISIBLE);
            }

            @SuppressWarnings("deprecation")
            @Override
            protected String doInBackground(String... params) {
                try {
                    HttpClient client = new DefaultHttpClient();
                    HttpPost httpPost;
                    if (isEditMode)
                        httpPost = new HttpPost(URLUtils.editSubProfile);
                    else
                        httpPost = new HttpPost(URLUtils.addProfile);
                    entityBuilder = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE);

                    if (imagePath.isEmpty()) {
                        Log.e("doInBack_file", ": is empty");
                    } else {
                        File file = new File(imagePath);
                        Log.e("doInBack_file", "" + file);
                        entityBuilder.addPart("picture", new FileBody(file, "application/octet"));

                        Log.d("EDIT USER PROFILE", "UPLOAD: file length = " + file.length());
                        Log.d("EDIT USER PROFILE", "UPLOAD: file exist = " + file.exists());
                    }

                    if (isEditMode) {
                        entityBuilder.addPart("sub_profile_id", new StringBody(getIntent().getStringExtra("id")));
                    }

                    entityBuilder.addPart("name", new StringBody(name));
                    entityBuilder.addPart("id", new StringBody(SharedPref.getKey(AddProfileActivity.this, "ID")));
                    entityBuilder.addPart("token", new StringBody(SharedPref.getKey(AddProfileActivity.this, "TOKEN")));


                    httpPost.setEntity(entityBuilder);
                    HttpResponse response = client.execute(httpPost);
                    HttpEntity httpEntity = response.getEntity();

                    if (response.getStatusLine().getStatusCode() == 200) {
                        String res = EntityUtils.toString(httpEntity);
                        Log.e("res", "" + res);
                        return res;
                    } else {
                        return "Error";
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                }

                return null;
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                progressBar.setVisibility(View.GONE);

                if (s.equalsIgnoreCase("Error")) {
                    UIUtils.showToast(AddProfileActivity.this, R.string.con_timeout);
                } else {
                    try {
                        JSONObject object = new JSONObject(s);
                        Log.i("object", "" + object);
                        String result = object.optString("success");
                        if (result.equalsIgnoreCase("true")) {
                            onBackPressed();

                        } else {
                            UIUtils.showToastMsg(AddProfileActivity.this, object.optString("error_messages"));
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        }.execute();
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(this, ManageProfileActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        this.finish();
    }
}
