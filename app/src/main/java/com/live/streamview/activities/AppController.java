package com.live.streamview.activities;

import android.app.Application;
import android.content.Context;
import android.support.multidex.MultiDex;
import android.view.Menu;

import java.util.ArrayList;


public class AppController extends Application{


    public static final String TAG = AppController.class.getSimpleName();
    private static double total = 0;
    public static ArrayList<Menu> menuListGlobal = new ArrayList<Menu>();


    private static AppController mInstance;

    public static double getTotal() {
        return total;
    }

    public static void setTotal(double total) {
        AppController.total = total;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;

    }

    public static synchronized AppController getInstance() {
        return mInstance;
    }


    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);

    }


}

