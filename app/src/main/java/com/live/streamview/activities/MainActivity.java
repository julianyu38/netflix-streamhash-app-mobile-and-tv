package com.live.streamview.activities;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.facebook.CallbackManager;
import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginManager;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.live.streamview.fragments.CommonCateFragment;
import com.live.streamview.fragments.CommonFragment;
import com.live.streamview.fragments.History;
import com.live.streamview.fragments.MyList;
import com.live.streamview.fragments.PaymentsFragment;
import com.live.streamview.fragments.Profile;
import com.live.streamview.fragments.SettingsFragment;
import com.live.streamview.fragments.SpamFragment;
import com.live.streamview.R;
import com.live.streamview.utils.ConnectionHelper;
import com.live.streamview.utils.ListUtils;
import com.live.streamview.utils.PostHelper;
import com.live.streamview.utils.SharedPref;
import com.live.streamview.utils.UIUtils;
import com.live.streamview.utils.URLUtils;
import com.mikhaellopez.circularimageview.CircularImageView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

@SuppressWarnings("deprecation")
public class MainActivity extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener,
                                                                    View.OnClickListener {

    public static final String UIUpdateEventFilter = "UIUpdate_event";
    public static FragmentManager fragmentManager;
    //public static MainActivity mainActivity = null;
    private boolean doubleBackToExitPressedOnce = false;

    private ImageView nav_home, nav_myList, nav_cate, nav_history, nav_span, nav_setting, nav_logout, nav_payments;
    private ImageView homeicon, myListicon, cateicon, historyicon, spamicon, settingsicon, logouticon, paymentsicon;

    private ListView listView;
    private ImageButton addProfile;
    private ImageView title;
    private MainCateAdapter mainCateAdapter;
    private DrawerLayout drawer;
    boolean status = true;
    private String ID = "", TOKEN = "";
    Fragment fragment = null;
    FragmentManager manager = null;
    @SuppressLint("CommitTransaction")
    FragmentTransaction transaction = null;

    private GoogleApiClient googleApiClient;
    private CallbackManager callbackManager;

    TextView name;
    CircularImageView circularImageView;

    private BroadcastReceiver UIUpdateReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            name.setText(SharedPref.getKey(MainActivity.this, "NAME"));
            Glide.with(MainActivity.this).load(SharedPref.getKey(MainActivity.this, "PROFILE_IMAGE")).
                    error(R.drawable.ic_profile).crossFade(50).centerCrop().into(circularImageView);
        }
    };

    @SuppressLint("CommitTransaction")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(MainActivity.this);
        AppEventsLogger.activateApp(AppController.getInstance());
        callbackManager = CallbackManager.Factory.create();
        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("");
        toolbar.setTitleTextColor(Color.WHITE);
        setSupportActionBar(toolbar);

        if (Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }

        ID = SharedPref.getKey(MainActivity.this, "ID");
        TOKEN = SharedPref.getKey(MainActivity.this, "TOKEN");

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .requestId()
                .requestProfile()
                .build();

        googleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();

        title = (ImageView) findViewById(R.id.title);

        LinearLayout home, myList, cate, history, spam, setting, payments, logout;
        home = (LinearLayout) findViewById(R.id.home);
        myList = (LinearLayout) findViewById(R.id.my_ist);
        cate = (LinearLayout) findViewById(R.id.category);
        history = (LinearLayout) findViewById(R.id.history);
        spam = (LinearLayout) findViewById(R.id.spam);
        payments = (LinearLayout) findViewById(R.id.payments);
        setting = (LinearLayout) findViewById(R.id.settings);
        logout = (LinearLayout) findViewById(R.id.logout);

        homeicon = (ImageView) findViewById(R.id.homeicon);
        myListicon = (ImageView) findViewById(R.id.mylisticon);
        cateicon = (ImageView) findViewById(R.id.cateicon);
        historyicon = (ImageView) findViewById(R.id.historyicon);
        spamicon = (ImageView) findViewById(R.id.spamicon);
        paymentsicon = (ImageView) findViewById(R.id.paymentsicon);
        settingsicon = (ImageView) findViewById(R.id.settingsicon);
        logouticon = (ImageView) findViewById(R.id.logouticon);

        addProfile = (ImageButton) findViewById(R.id.addProfile);
        addProfile.setOnClickListener(this);

        home.setOnClickListener(this);
        myList.setOnClickListener(this);
        cate.setOnClickListener(this);
        history.setOnClickListener(this);
        spam.setOnClickListener(this);
        payments.setOnClickListener(this);
        setting.setOnClickListener(this);
        logout.setOnClickListener(this);

        nav_home = (ImageView) findViewById(R.id.nav_home);
        nav_myList = (ImageView) findViewById(R.id.nav_my_list);
        nav_cate = (ImageView) findViewById(R.id.nav_category);
        nav_history = (ImageView) findViewById(R.id.nav_history);
        nav_span = (ImageView) findViewById(R.id.nav_spam);
        nav_payments = (ImageView) findViewById(R.id.nav_payments);
        nav_setting = (ImageView) findViewById(R.id.nav_settings);
        nav_logout = (ImageView) findViewById(R.id.nav_logout);

        nav_home.setVisibility(View.GONE);
        nav_myList.setVisibility(View.GONE);
        nav_cate.setVisibility(View.GONE);
        nav_history.setVisibility(View.GONE);
        nav_span.setVisibility(View.GONE);
        nav_payments.setVisibility(View.GONE);
        nav_setting.setVisibility(View.GONE);
        nav_logout.setVisibility(View.GONE);

        if (getIntent().hasExtra("changeToPayment")) {
            showNavigation(getIntent().getStringExtra("navigation"));
            fragment = new PaymentsFragment();
            toolbar.setTitle("Payments");
        }
        else {
            showNavigation("home");
            fragment = new CommonFragment();
        }
        manager = getSupportFragmentManager();
        transaction = manager.beginTransaction();
        transaction.replace(R.id.content, fragment);
        transaction.commit();
        fragmentManager = getSupportFragmentManager();

        NavigationView navigationView = (NavigationView) findViewById(R.id.navigation_view);
        name = (TextView) navigationView.findViewById(R.id.profile_name);
        circularImageView = (CircularImageView) navigationView.findViewById(R.id.profile_image);
        circularImageView.setOnClickListener(this);
        name.setText(SharedPref.getKey(MainActivity.this, "NAME"));
        Glide.with(MainActivity.this).load(SharedPref.getKey(MainActivity.this, "PROFILE_IMAGE")).error(R.drawable.ic_profile).crossFade(50).centerCrop().into(circularImageView);
        listView = (ListView) navigationView.findViewById(R.id.cate_list);
        listView.setVisibility(View.GONE);
        drawer = (DrawerLayout) findViewById(R.id.drawer);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.drawer_open, R.string.drawer_close);
        assert drawer != null;
        //noinspection deprecation
        drawer.setDrawerListener(toggle);
        drawer.setStatusBarBackgroundColor(Color.WHITE);
        toggle.syncState();
        getMainCategory();
        LocalBroadcastManager.getInstance(this).registerReceiver(UIUpdateReceiver, new IntentFilter(UIUpdateEventFilter));
    }

    @Override
    protected void onDestroy() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(UIUpdateReceiver);
        super.onDestroy();
    }

    private void getMainCategory() {
        new AsyncTask<JSONObject, JSONObject, JSONObject>() {
            JSONArray category_array;


            @Override
            protected JSONObject doInBackground(JSONObject... jsonObjects) {

                JSONObject jsonObject = new JSONObject();
                try {
                    jsonObject.put("id", ID);
                    jsonObject.put("token", TOKEN);
                    PostHelper postHelper = new PostHelper(MainActivity.this);
                    return postHelper.Post(URLUtils.categories, jsonObject.toString());
                } catch (JSONException | IOException e) {
                    e.printStackTrace();
                }

                return null;
            }

            @Override
            protected void onPostExecute(JSONObject jsonObject) {
                super.onPostExecute(jsonObject);
                Log.e("mahi", "cate response" + jsonObject);
                if (jsonObject != null) {
                    if (jsonObject.optString("success").equalsIgnoreCase("true")) {
                        category_array = jsonObject.optJSONArray("categories");
                        mainCateAdapter = new MainCateAdapter(category_array);
                        listView.setAdapter(mainCateAdapter);
                        ListUtils.getListViewSize(listView);
                    } else {
                        UIUtils.showToastMsg(MainActivity.this, jsonObject.optString("error"));
                        if (jsonObject.optString("error_code").equalsIgnoreCase("104")) {
                            Intent intent = new Intent(MainActivity.this, Login.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent);
                        }
                    }
                } else {
                    UIUtils.showToast(MainActivity.this, R.string.con_timeout);
                }
            }
        }.execute();
    }

    @SuppressLint("CommitTransaction")
    @Override
    public void onClick(View view) {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);

        switch (view.getId()) {
            case R.id.profile_image:
                if (ConnectionHelper.isConnectingToInternet(MainActivity.this)) {
                    drawer.closeDrawer(GravityCompat.START);
                    toolbar.setTitle("Profile");
                    title.setVisibility(View.GONE);
                    fragment = new Profile();
                    manager = getSupportFragmentManager();
                    transaction = manager.beginTransaction();
                    transaction.replace(R.id.content, fragment);
                    transaction.commit();
                    fragmentManager = getSupportFragmentManager();

                } else {
                    UIUtils.showNetworkAlert(MainActivity.this, R.string.check_network);
                }
                break;
            case R.id.home:
                if (ConnectionHelper.isConnectingToInternet(MainActivity.this)) {
                    showNavigation("home");
//                    toolbar.setTitle("Home");
                    toolbar.setTitle("");
                    title.setVisibility(View.VISIBLE);
                    drawer.closeDrawer(GravityCompat.START);
                    fragment = new CommonFragment();
                    manager = getSupportFragmentManager();
                    transaction = manager.beginTransaction();
                    transaction.replace(R.id.content, fragment);
                    transaction.commit();
                    fragmentManager = getSupportFragmentManager();

                } else {
                    UIUtils.showNetworkAlert(MainActivity.this, R.string.check_network);
                }
                break;
            case R.id.my_ist:
                showNavigation("myList");
                toolbar.setTitle("My List");
                title.setVisibility(View.GONE);
                drawer.closeDrawer(GravityCompat.START);
                fragment = new MyList();
                manager = getSupportFragmentManager();
                transaction = manager.beginTransaction();
                transaction.replace(R.id.content, fragment);
                transaction.commit();
                fragmentManager = getSupportFragmentManager();
                break;
            case R.id.category:
                /*showNavigation("category");
                toolbar.setTitle("Category");
                drawer.closeDrawer(GravityCompat.START);
                fragment = new MainCategory();
                manager = getSupportFragmentManager();
                transaction = manager.beginTransaction();
                transaction.replace(R.id.content, fragment);
                transaction.commit();
                fragmentManager = getSupportFragmentManager();*/
                if (status) {
                    listView.setVisibility(View.VISIBLE);
                    status = false;
                } else {
                    listView.setVisibility(View.GONE);
                    status = true;
                }

                break;
            case R.id.history:
                showNavigation("history");
                toolbar.setTitle("History");
                title.setVisibility(View.GONE);
                drawer.closeDrawer(GravityCompat.START);
                fragment = new History();
                manager = getSupportFragmentManager();
                transaction = manager.beginTransaction();
                transaction.replace(R.id.content, fragment);
                transaction.commit();
                fragmentManager = getSupportFragmentManager();
                break;
            case R.id.spam:
                showNavigation("spam");
                toolbar.setTitle("Spam");
                title.setVisibility(View.GONE);
                drawer.closeDrawer(GravityCompat.START);
                fragment = new SpamFragment();
                manager = getSupportFragmentManager();
                transaction = manager.beginTransaction();
                transaction.replace(R.id.content, fragment);
                transaction.commit();
                fragmentManager = getSupportFragmentManager();
                break;
            case R.id.payments:
                showNavigation("payments");
                toolbar.setTitle("Payments");
                title.setVisibility(View.GONE);
                drawer.closeDrawer(GravityCompat.START);
                fragment = new PaymentsFragment();
                manager = getSupportFragmentManager();
                transaction = manager.beginTransaction();
                transaction.replace(R.id.content, fragment);
                transaction.commit();
                fragmentManager = getSupportFragmentManager();
                break;
            case R.id.settings:
                showNavigation("setting");
                toolbar.setTitle("Settings");
                title.setVisibility(View.GONE);
                drawer.closeDrawer(GravityCompat.START);
                fragment = new SettingsFragment();
                manager = getSupportFragmentManager();
                transaction = manager.beginTransaction();
                transaction.replace(R.id.content, fragment);
                transaction.commit();
                fragmentManager = getSupportFragmentManager();
                break;
            case R.id.logout:
                showNavigation("logout");
                drawer.closeDrawer(GravityCompat.START);
                if (ConnectionHelper.isConnectingToInternet(MainActivity.this)) {
                    if (SharedPref.getKey(MainActivity.this, "login_type").equalsIgnoreCase("facebook")) {
                        LoginManager loginManager = LoginManager.getInstance();
                        loginManager.logOut();
                        Intent logIntent = new Intent(MainActivity.this, Login.class);
                        logIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        SharedPref.putKey(MainActivity.this, "login_type", "");
                        startActivity(logIntent);
                        finish();

                    } else if (SharedPref.getKey(MainActivity.this, "login_type").equalsIgnoreCase("google")) {
                        Auth.GoogleSignInApi.revokeAccess(googleApiClient).setResultCallback(new ResultCallback<Status>() {
                            @Override
                            public void onResult(@NonNull Status status) {
                                Log.d("GOOGLESTATUS", status.toString());
                            }
                        });
                        Intent logIntent = new Intent(MainActivity.this, Login.class);
                        logIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        SharedPref.putKey(MainActivity.this, "login_type", "");
                        startActivity(logIntent);
                        finish();

                    } else if (SharedPref.getKey(MainActivity.this, "login_type").equalsIgnoreCase("Logged")) {
                        Intent logIntent = new Intent(MainActivity.this, Login.class);
                        logIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        SharedPref.putKey(MainActivity.this, "login_type", "");
                        startActivity(logIntent);
                        finish();

                    }
                } else {
                    UIUtils.showNetworkAlert(MainActivity.this, R.string.check_network);
                }
                break;
            case R.id.addProfile:
                startActivity(new Intent(this, ManageProfileActivity.class));
                break;
        }

    }

    private void showNavigation(String value) {
        setDefaultIconColors();
        if (value.equalsIgnoreCase("home")) {
            nav_home.setVisibility(View.VISIBLE);
            homeicon.setColorFilter(ContextCompat.getColor(this, R.color.colorAccent), PorterDuff.Mode.SRC_IN);
            nav_myList.setVisibility(View.GONE);
            nav_cate.setVisibility(View.GONE);
            nav_history.setVisibility(View.GONE);
            nav_payments.setVisibility(View.GONE);
            nav_setting.setVisibility(View.GONE);
            nav_logout.setVisibility(View.GONE);
            nav_span.setVisibility(View.GONE);
        } else if (value.equalsIgnoreCase("myList")) {
            nav_home.setVisibility(View.GONE);
            nav_myList.setVisibility(View.VISIBLE);
            myListicon.setColorFilter(ContextCompat.getColor(this, R.color.colorAccent), PorterDuff.Mode.SRC_IN);
            nav_cate.setVisibility(View.GONE);
            nav_payments.setVisibility(View.GONE);
            nav_history.setVisibility(View.GONE);
            nav_setting.setVisibility(View.GONE);
            nav_logout.setVisibility(View.GONE);
            nav_span.setVisibility(View.GONE);
        } else if (value.equalsIgnoreCase("category")) {
            nav_home.setVisibility(View.GONE);
            nav_myList.setVisibility(View.GONE);
            nav_cate.setVisibility(View.VISIBLE);
            cateicon.setColorFilter(ContextCompat.getColor(this, R.color.colorAccent), PorterDuff.Mode.SRC_IN);
            nav_history.setVisibility(View.GONE);
            nav_payments.setVisibility(View.GONE);
            nav_setting.setVisibility(View.GONE);
            nav_logout.setVisibility(View.GONE);
            nav_span.setVisibility(View.GONE);
        } else if (value.equalsIgnoreCase("history")) {
            nav_home.setVisibility(View.GONE);
            nav_myList.setVisibility(View.GONE);
            nav_cate.setVisibility(View.GONE);
            nav_span.setVisibility(View.GONE);
            nav_payments.setVisibility(View.GONE);
            nav_history.setVisibility(View.VISIBLE);
            historyicon.setColorFilter(ContextCompat.getColor(this, R.color.colorAccent), PorterDuff.Mode.SRC_IN);
            nav_setting.setVisibility(View.GONE);
            nav_logout.setVisibility(View.GONE);
        }  else if (value.equalsIgnoreCase("spam")) {
            nav_home.setVisibility(View.GONE);
            nav_myList.setVisibility(View.GONE);
            nav_cate.setVisibility(View.GONE);
            nav_payments.setVisibility(View.GONE);
            nav_history.setVisibility(View.GONE);
            nav_span.setVisibility(View.VISIBLE);
            spamicon.setColorFilter(ContextCompat.getColor(this, R.color.colorAccent), PorterDuff.Mode.SRC_IN);
            nav_setting.setVisibility(View.GONE);
            nav_logout.setVisibility(View.GONE);
        } else if (value.equalsIgnoreCase("payments")) {
            nav_home.setVisibility(View.GONE);
            nav_myList.setVisibility(View.GONE);
            nav_span.setVisibility(View.GONE);
            nav_cate.setVisibility(View.GONE);
            nav_payments.setVisibility(View.VISIBLE);
            paymentsicon.setColorFilter(ContextCompat.getColor(this, R.color.colorAccent), PorterDuff.Mode.SRC_IN);
            nav_history.setVisibility(View.GONE);
            nav_setting.setVisibility(View.GONE);
            nav_logout.setVisibility(View.GONE);
        } else if (value.equalsIgnoreCase("setting")) {
            nav_home.setVisibility(View.GONE);
            nav_myList.setVisibility(View.GONE);
            nav_cate.setVisibility(View.GONE);
            nav_payments.setVisibility(View.GONE);
            nav_history.setVisibility(View.GONE);
            nav_span.setVisibility(View.GONE);
            nav_setting.setVisibility(View.VISIBLE);
            settingsicon.setColorFilter(ContextCompat.getColor(this, R.color.colorAccent), PorterDuff.Mode.SRC_IN);
            nav_logout.setVisibility(View.GONE);
        } else if (value.equalsIgnoreCase("logout")) {
            nav_home.setVisibility(View.GONE);
            nav_myList.setVisibility(View.GONE);
            nav_cate.setVisibility(View.GONE);
            nav_span.setVisibility(View.GONE);
            nav_payments.setVisibility(View.GONE);
            nav_history.setVisibility(View.GONE);
            nav_setting.setVisibility(View.GONE);
            nav_logout.setVisibility(View.VISIBLE);
            logouticon.setColorFilter(ContextCompat.getColor(this, R.color.colorAccent), PorterDuff.Mode.SRC_IN);
        }
    }

    private void setDefaultIconColors() {
        homeicon.setColorFilter(ContextCompat.getColor(this, R.color.white), PorterDuff.Mode.SRC_IN);
        myListicon.setColorFilter(ContextCompat.getColor(this, R.color.white), PorterDuff.Mode.SRC_IN);
        cateicon.setColorFilter(ContextCompat.getColor(this, R.color.white), PorterDuff.Mode.SRC_IN);
        historyicon.setColorFilter(ContextCompat.getColor(this, R.color.white), PorterDuff.Mode.SRC_IN);
        spamicon.setColorFilter(ContextCompat.getColor(this, R.color.white), PorterDuff.Mode.SRC_IN);
        paymentsicon.setColorFilter(ContextCompat.getColor(this, R.color.white), PorterDuff.Mode.SRC_IN);
        settingsicon.setColorFilter(ContextCompat.getColor(this, R.color.white), PorterDuff.Mode.SRC_IN);
        logouticon.setColorFilter(ContextCompat.getColor(this, R.color.white), PorterDuff.Mode.SRC_IN);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            if (doubleBackToExitPressedOnce) {
                Intent intent = new Intent(Intent.ACTION_MAIN);
                intent.addCategory(Intent.CATEGORY_HOME);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            } else {
                UIUtils.showToastMsg(MainActivity.this, "Press the back button once again to close the application.");
                doubleBackToExitPressedOnce = true;
            }
        }
    }

    @Override
    protected void onStart() {
        super.onStart();

    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        isFinishing();

    }

    @Override
    protected void onResume() {
        super.onResume();
        this.doubleBackToExitPressedOnce = false;
        ConnectionHelper.isConnectingToInternet(MainActivity.this);
    }

    private class MainCateAdapter extends BaseAdapter {
        JSONArray dataSet;

        public MainCateAdapter(JSONArray category_array) {
            this.dataSet = category_array;
        }

        @Override
        public int getCount() {
            return dataSet.length();
        }

        @Override
        public Object getItem(int i) {
            return null;
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

        @Override
        public View getView(final int i, View view, ViewGroup viewGroup) {
            @SuppressLint("ViewHolder") View myView = LayoutInflater.from(MainActivity.this).inflate(R.layout.nav_cate_list_item, viewGroup, false);
            TextView name = (TextView) myView.findViewById(R.id.nav_cate_name);
            name.setText(dataSet.optJSONObject(i).optString("name"));

            myView.setOnClickListener(new View.OnClickListener() {
                @SuppressLint("CommitTransaction")
                @Override
                public void onClick(View view) {
                    showNavigation("category");
                    drawer.closeDrawer(GravityCompat.START);
                    String ID = dataSet.optJSONObject(i).optString("id");
                    Fragment fragment = new CommonCateFragment();
                    Bundle bundle = new Bundle();
                    bundle.putString("id", ID);
                    bundle.putString("sub_cate", "sub");
                    bundle.putString("name", dataSet.optJSONObject(i).optString("name"));
                    fragment.setArguments(bundle);
                    manager = getSupportFragmentManager();
                    transaction = manager.beginTransaction();
                    transaction.replace(R.id.content, fragment);
                    transaction.commit();
                    fragmentManager = getSupportFragmentManager();
                }
            });

            return myView;
        }
    }
}
