package com.live.streamview.activities;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.live.streamview.R;
import com.live.streamview.utils.PostHelper;
import com.live.streamview.utils.SharedPref;
import com.live.streamview.utils.UIUtils;
import com.live.streamview.utils.URLUtils;
import com.live.streamview.adapters.SubscriptionAdapter;
import com.live.streamview.models.Subscription;
import com.paypal.android.sdk.payments.PayPalConfiguration;
import com.paypal.android.sdk.payments.PayPalPayment;
import com.paypal.android.sdk.payments.PayPalService;
import com.paypal.android.sdk.payments.PaymentActivity;
import com.paypal.android.sdk.payments.PaymentConfirmation;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class PlansActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener,
                                        SubscriptionAdapter.OnSubscriptionInterface{

    private int type = 0;
    private ProgressBar progressBar;
    private RelativeLayout errorLayout;
    private RecyclerView plans;
    private List<Subscription> subscriptionList;
    private SwipeRefreshLayout swipe;
    private SubscriptionAdapter adapter;
    private Subscription subscription;
    private Dialog dialog;

    public static final int PAYPAL_REQUEST_CODE = 200;

    public static PayPalConfiguration config = new PayPalConfiguration()
            .environment(PayPalConfiguration.ENVIRONMENT_SANDBOX)
            .clientId(URLUtils.PayPalConfig.PAYPAL_CLIENT_ID);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_plans);
        Toolbar toolbar = (Toolbar) findViewById(R.id.forgot_in_toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_black_24dp);
        toolbar.setTitleTextColor(Color.WHITE);
        Intent intent = getIntent();
        Intent payintent = new Intent(this, PayPalService.class);
        payintent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);
        startService(payintent);
        if (intent != null) {
            toolbar.setTitle(intent.getStringExtra("title"));
            type = intent.getIntExtra("type", 0);
        }
        setSupportActionBar(toolbar);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        progressBar = (ProgressBar) findViewById(R.id.progress_bar);
        progressBar.setVisibility(View.GONE);
        errorLayout = (RelativeLayout) findViewById(R.id.error_layout);
        errorLayout.setVisibility(View.GONE);
        swipe = (SwipeRefreshLayout) findViewById(R.id.swipe);
        subscriptionList = new ArrayList<>();
        swipe.setOnRefreshListener(this);
        swipe.post(new Runnable() {
            @Override
            public void run() {
                getSubscriptions(0);
            }
        });
        plans = (RecyclerView) findViewById(R.id.plans);
        plans.setHasFixedSize(true);
        plans.setItemAnimator(new DefaultItemAnimator());
        plans.setLayoutManager(new LinearLayoutManager(this));
        adapter = new SubscriptionAdapter(this, subscriptionList);
        plans.setAdapter(adapter);
        adapter.setOnSubscriptionListener(this);
    }

    private void getSubscriptions(final int skip) {
        new AsyncTask<JSONObject, JSONObject, JSONObject>() {

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                progressBar.setVisibility(View.VISIBLE);
            }

            @Override
            protected JSONObject doInBackground(JSONObject... jsonObjects) {
                try {
                    PostHelper postHelper = new PostHelper(PlansActivity.this);
                    JSONObject object = new JSONObject();
                    object.put("id", SharedPref.getKey(PlansActivity.this, "ID"));
                    object.put("token", SharedPref.getKey(PlansActivity.this, "TOKEN"));
                    object.put("skip", skip);
                    if (type == 1) {
                        return postHelper.Post(URLUtils.viewPlans, object.toString());
                    }
                    else if (type == 2){
                        return postHelper.Post(URLUtils.myPlans, object.toString());
                    }
                }
                catch (JSONException | IOException e) {
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(JSONObject object) {
                super.onPostExecute(object);
                Log.e("Response", object.toString());
                progressBar.setVisibility(View.GONE);
                if (swipe.isRefreshing()) swipe.setRefreshing(false);
                if (object != null) {
                    String result = object.optString("success");
                    if (result.equalsIgnoreCase("true")) {
                        JSONArray data = object.optJSONArray("data");
                        for (int i = 0 ; i < data.length(); i++) {
                            JSONObject obj = data.optJSONObject(i);
                            Subscription subscription =
                                    new Subscription(obj.optString("subscription_id"),
                                            obj.optString("title"),
                                            obj.optString("description"),
                                            obj.optString("plan"),
                                            obj.optString("amount"),
                                            obj.optString("currency"),
                                            obj.optString("popular_status"),
                                            obj.optString("no_of_account"),
                                            obj.optString("expiry_date"));
                            if (obj.optInt("user_subscription_id") == 1) {
                                subscription.setMine(true);
                            }
                            else{
                                subscription.setMine(false);
                            }
                            subscriptionList.add(subscription);
                        }
                        adapter.notifyDataSetChanged();
                        if (subscriptionList.size() > 0 ) {
                            errorLayout.setVisibility(View.GONE);
                            plans.setVisibility(View.VISIBLE);
                        }
                        else{
                            errorLayout.setVisibility(View.VISIBLE);
                            plans.setVisibility(View.GONE);
                        }
                    }
                }
                else{
                    UIUtils.showToastMsg(PlansActivity.this, object.optString("error"));
                    if (object.optString("error_code").equalsIgnoreCase("104")) {
                        Intent intent = new Intent(PlansActivity.this, Login.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                        finish();
                    }
                }
            }
        }.execute();
    }


    @Override
    public void onRefresh() {
        if (subscriptionList != null && subscriptionList.size() > 0) {
            subscriptionList.clear();
        }
        getSubscriptions(0);
    }

    @Override
    public void showDialog(final Subscription subscription) {
        this.subscription = subscription;
        dialog = new Dialog(this, R.style.AppTheme);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_invoice);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        ImageButton close = (ImageButton) dialog.findViewById(R.id.close);
        ImageButton paypal = (ImageButton) dialog.findViewById(R.id.paypal);
        ImageButton stripe = (ImageButton) dialog.findViewById(R.id.stripe);
        TextView title = (TextView) dialog.findViewById(R.id.title);
        TextView price = (TextView) dialog.findViewById(R.id.price);
        TextView months = (TextView) dialog.findViewById(R.id.months);
        title.setText(subscription.getTitle());
        price.setText(subscription.getAmount());
        months.setText(subscription.getMonths());
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.cancel();
            }
        });
        paypal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                if (Double.parseDouble(subscription.getOriginalAmount()) == 0) {
                    showPaymentConfirmationDialog("Free trial Plan");
                    makeAPing(subscription.getId(), "Free trial Plan");
                }
                else {
                    getPaypalPayment(subscription.getOriginalAmount(), subscription.getTitle());
                }
            }
        });
        stripe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                makeStripePayment(subscription.getId());
            }
        });
        dialog.show();
    }

    private void makeStripePayment(final String id) {
        new AsyncTask<JSONObject, JSONObject, JSONObject>() {
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                progressBar.setVisibility(View.VISIBLE);
            }

            @Override
            protected JSONObject doInBackground(JSONObject... jsonObjects) {
                try {
                    JSONObject object = new JSONObject();
                    object.put("id", SharedPref.getKey(PlansActivity.this, "ID"));
                    object.put("token", SharedPref.getKey(PlansActivity.this, "TOKEN"));
                    object.put("sub_profile_id", SharedPref.getKey(PlansActivity.this, "SUB_PROFILE"));
                    object.put("subscription_id", id);
                    PostHelper postHelper = new PostHelper(PlansActivity.this);
                    return postHelper.Post(URLUtils.stripePayment, object.toString());
                }
                catch (JSONException | IOException e) {
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(JSONObject object) {
                super.onPostExecute(object);
                progressBar.setVisibility(View.VISIBLE);
                if (object != null) {
                    if (object.optString("success").equals("true")) {
                        UIUtils.showToastMsg(PlansActivity.this, object.optString("message"));
                        JSONObject data = object.optJSONObject("data");
                        SharedPref.putKey(PlansActivity.this, "TOKEN", data.optString("token"));
                    }
                    else{
                        if (object.optInt("error_code") == 901) {
                            UIUtils.showToastMsg(PlansActivity.this, object.optString("error_messages"));
                            Intent intent = new Intent(PlansActivity.this, MainActivity.class);
                            intent.putExtra("changeToPayment", true);
                            intent.putExtra("navigation", "payments");
                            startActivity(intent);
                        }
                    }
                }
            }
        }.execute();
    }

    private void getPaypalPayment(String amount, String title) {
        PayPalPayment payment = new PayPalPayment(new BigDecimal(String.valueOf(amount)), "USD", title,
                PayPalPayment.PAYMENT_INTENT_SALE);
        Intent intent = new Intent(this, PaymentActivity.class);
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);
        intent.putExtra(PaymentActivity.EXTRA_PAYMENT, payment);
        startActivityForResult(intent, PAYPAL_REQUEST_CODE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PAYPAL_REQUEST_CODE){
            if (resultCode == Activity.RESULT_OK){
                PaymentConfirmation confirm = data.getParcelableExtra(PaymentActivity.EXTRA_RESULT_CONFIRMATION);

                if (confirm != null){
                    try {
                        String paymentDetails = confirm.toJSONObject().toString(4);
                        Log.d("PaypalPayment", paymentDetails);
                        showPaymentConfirmationDialog(paymentDetails);
                        SharedPref.putKey(PlansActivity.this, "user_type", "1");
                    }
                    catch (JSONException e){
                        e.printStackTrace();
                    }
                }
            }
            else if (resultCode == Activity.RESULT_CANCELED){
                if (dialog != null && dialog.isShowing()) { dialog.cancel(); }
            }
        }
        else if (resultCode == PaymentActivity.RESULT_EXTRAS_INVALID){
            Log.e("PaypalError", "Invalid Payment or Paypal Configuration was submitted");
        }
    }

    private void showPaymentConfirmationDialog(String paymentDetails){
        final Dialog dialog = new Dialog(this, R.style.AppTheme);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_payment_confirmation);
        dialog.setCancelable(false);
        TextView paidamount = (TextView) dialog.findViewById(R.id.amount);
        TextView status = (TextView) dialog.findViewById(R.id.status);
        TextView paidid = (TextView) dialog.findViewById(R.id.paymentid);
        try {
            JSONObject details = new JSONObject(paymentDetails);
            paidamount.setText(subscription.getAmount());
            status.setText(details.optJSONObject("response").optString("state"));
            paidid.setText(details.optJSONObject("response").optString("id"));
            if (status.getText().toString().equalsIgnoreCase("approved")) {
                makeAPing(subscription.getId(), details.optJSONObject("response").optString("id"));
                SharedPref.putKey(PlansActivity.this, "user_type", "1");
            }
            else{
                UIUtils.showToastMsg(PlansActivity.this, "Payment unsuccessful");
            }
        }
        catch (JSONException e){
            e.printStackTrace();
            paidamount.setText(subscription.getAmount());
            status.setText("Approved");
            paidid.setText(paymentDetails);
            SharedPref.putKey(PlansActivity.this, "user_type", "1");
        }
        Button done = (Button) dialog.findViewById(R.id.button_start);
        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (dialog != null && dialog.isShowing()) dialog.cancel();
            }
        });
        dialog.show();
    }

    private void makeAPing(final String id, final String payment_id){
        new AsyncTask<JSONObject, JSONObject, JSONObject>() {
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                progressBar.setVisibility(View.VISIBLE);
            }

            @Override
            protected JSONObject doInBackground(JSONObject... jsonObjects) {
                try {
                    JSONObject object = new JSONObject();
                    object.put("id", SharedPref.getKey(PlansActivity.this, "ID"));
                    object.put("token", SharedPref.getKey(PlansActivity.this, "TOKEN"));
                    object.put("payment_id", payment_id);
                    object.put("subscription_id", id);
                    PostHelper postHelper = new PostHelper(PlansActivity.this);
                    return postHelper.Post(URLUtils.paypalPayment, object.toString());
                }
                catch (JSONException | IOException e) {
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(JSONObject object) {
                super.onPostExecute(object);
                if (object != null) {
                    if (object.optString("success").equals("true")) {
                        UIUtils.showToastMsg(PlansActivity.this, object.optString("message"));
                        JSONObject data = object.optJSONObject("data");
                        SharedPref.putKey(PlansActivity.this, "TOKEN", data.optString("token"));
                    }
                }
            }
        }.execute();
    }
}
