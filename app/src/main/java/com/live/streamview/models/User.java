package com.live.streamview.models;

import java.io.Serializable;

/**
 * Created by codegama on 16/10/17.
 */

public class User implements Serializable {

    private String id;
    private String name;
    private String image;
    private boolean main;
    private boolean last;

    public User(String id, String name, String image, boolean main) {
        this.id = id;
        this.name = name;
        this.image = image;
        this.main = main;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public boolean isMain() {
        return main;
    }

    public void setMain(boolean main) {
        this.main = main;
    }

    public boolean isLast() {
        return last;
    }

    public void setLast(boolean last) {
        this.last = last;
    }
}
