package com.live.streamview.models;

import java.io.Serializable;

/**
 * Created by codegama on 14/10/17.
 */

public class Subscription implements Serializable{

    private String id;
    private String title;
    private String description;
    private String months;
    private String amount;
    private String currency;
    private String status;
    private String accounts;
    private String expires;
    private boolean mine;

    public Subscription(String id, String title, String description, String months, String amount, String currency, String status, String accounts, String expires) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.months = months;
        this.amount = amount;
        this.currency = currency;
        this.status = status;
        this.accounts = accounts;
        this.expires = expires;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getMonths() {
        return months + " Months";
    }

    public void setMonths(String months) {
        this.months = months;
    }

    public String getAmount() {
        return getCurrency() + amount;
    }

    public String getOriginalAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getAccounts() {
        return accounts + " Accounts";
    }

    public void setAccounts(String accounts) {
        this.accounts = accounts;
    }

    public String getExpires() {
        if (expires.isEmpty()) return "";
        return "Expires on " + expires;
    }

    public void setExpires(String expires) {
        this.expires = expires;
    }

    public boolean isMine() {
        return mine;
    }

    public void setMine(boolean mine) {
        this.mine = mine;
    }
}
